#!/usr/bin/env python
# -*- coding:utf-8 -*-
from __future__ import print_function
import os
import sys

from mobileApp import start, iOSBuilder, Mode, Cocos2dxAndroidBuilder, BuilderInfo
from utils import prepare_dir, wildone, file_head, AssertNotNull, file_txt


class Cocos3BI(BuilderInfo):
    def __init__(self, title, version, basedir='./'):
        BuilderInfo.__init__(self)
        self.title = title
        self.bundleID = None
        self.version = version
        self.basedir = basedir
        self.output = os.path.join(basedir, 'bin')
        self.set('proj.android', '%s/proj.android/' % basedir)
        self.set('proj.ios', '%s/proj.ios_mac/' % basedir)
        self.set('project.pbxproj', wildone('%s/proj.ios_mac/*.xcodeproj/project.pbxproj' % basedir))
        self.set('Info.plist', '%s/proj.ios_mac/ios/Info.plist' % basedir)
        # 默认的目录
        for mode in [Mode.DEBUG, Mode.RELEASE, Mode.STORE]:
            key_path = os.path.join(basedir, 'key', 'ios', mode)
            bundleID = wildone(os.path.join(key_path, '*.bundleID'))
            codeSign = wildone(os.path.join(key_path, '*.codeSign'), fail=False)
            keychains = wildone(os.path.join(key_path, '*.keychain'), fail=False)
            provisioning = wildone(os.path.join(key_path, '*.mobileprovision'), fail=False)
            passwd = wildone(os.path.join(key_path, '*.passwd'), fail=False)
            version_file = wildone(os.path.join(key_path, '*.version'), fail=False)
            if version_file:
                _ = file_head(version_file, line_num=2)
                human_version = _[0]
                build_version = _[1]
            else:
                human_version = '1.0.'
                build_version = '1.0.'
            self.set('%s.human_version' % mode, human_version)
            self.set('%s.build_version' % mode, build_version)
            if keychains and provisioning and passwd:
                codeSign = file_head(codeSign)
                passwd = file_head(passwd)
                bundleID = file_head(bundleID)
                self.sign_info(mode, bundleID, codeSign, provisioning, keychains, passwd)
            else:
                self.sign_info(mode, bundleID, None, None, None, None)
            plist_template = '%s/key/ios/%s/plist.template' % (basedir, mode)
            if os.path.exists(plist_template):
                self.set('%s.plist' % mode, file_txt(plist_template))
            else:
                self.set('%s.plist' % mode, None)

    def sign_info(self, mode, bundleID, codeSign, provisioning, keychains, passwd):
        AssertNotNull("没有指定模式", mode)
        self.set('%s.bundleID' % mode, bundleID)
        self.set('%s.codeSign' % mode, codeSign)
        self.set('%s.provisioning' % mode, provisioning)
        self.set('%s.keychains' % mode, keychains)
        self.set('%s.keychains.passwd' % mode, passwd)


def main():
    if len(sys.argv) < 3:
        print("""\
Usage:
    Cocos3Builder.py <ios|android|all> <path> <title> <version>
""")
        exit(1)
    platform = sys.argv[1]
    path = os.path.abspath(sys.argv[2])
    title = sys.argv[3]
    version = sys.argv[4]
    bi = Cocos3BI(title, version, path)
    prepare_dir(bi.output)
    if platform == 'ios' or platform == 'all':
        # start(iOSBuilder(bi, verbose=True), Mode.DEBUG, clean_output=False)
        start(iOSBuilder(bi, verbose=True), Mode.ENTERPRISE, clean_output=False)
        start(iOSBuilder(bi, verbose=True), Mode.STORE, clean_output=False)

    if platform == 'android' or platform == 'all':
        start(Cocos2dxAndroidBuilder(bi, verbose=True), Mode.DEBUG, clean_output=False)
        start(Cocos2dxAndroidBuilder(bi, verbose=True), Mode.RELEASE, clean_output=False)


if __name__ == '__main__':
    main()
