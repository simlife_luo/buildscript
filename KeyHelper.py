#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
负责帮助项目选择最合适的证书
"""
import os
import shutil
import sys

from utils import wildone


def mkdirs(path):
    path = str(path).strip()
    if len(path) == 0:
        return
    if not os.path.exists(path):
        parent, cur = os.path.dirname(path), os.path.basename(path)
        mkdirs(parent)
        os.mkdir(cur)


def cp(orig_wild, dst):
    """
    泛拷贝文件
    :param orig_wild:
    :param dst:
    :return:
    :rtype bool
    """
    src = wildone(orig_wild, fail=False)
    if src is None:
        return False
    if os.path.isdir(dst):
        dst = os.path.join(dst, os.path.basename(src))
    shutil.copyfile(src, dst)
    return True


def run_ios(keys_root, key_dir, title, bundleID):
    out = os.path.join(key_dir, 'ios')
    mkdirs(out)

    def common(src, dst):
        cp(os.path.join(src, '*.bundleID'), dst)
        cp(os.path.join(src, '*.codeSign'), dst)
        cp(os.path.join(src, '*.keychain'), dst)
        cp(os.path.join(src, '*.passwd'), dst)
        if not cp(os.path.join(src, '*.mobileprovision'), dst):
            cp(wildone(os.path.join(src, '*%s*.mobileprovision' % title), fail=True), dst)
            if bundleID is not None:
                # todo: 校验 bundleID
                pass

    # DEBUG START
    mkdirs(os.path.join(key_dir, 'ios', 'debug'))
    debug_src = os.path.join(keys_root, 'ios', 'debug')
    debug_dst = os.path.join(out, 'debug')
    common(debug_src, debug_dst)
    # DEBUG END

    # RELEASE START
    mkdirs(os.path.join(key_dir, 'ios', 'release'))
    release_src = os.path.join(keys_root, 'ios', 'release')
    release_dst = os.path.join(out, 'release')
    common(release_src, release_dst)
    # RELEASE END

    # STORE START
    mkdirs(os.path.join(key_dir, 'ios', 'store'))
    store_src = os.path.join(keys_root, 'ios', 'store')
    store_dst = os.path.join(out, 'store')
    common(store_src, store_dst)
    # STORE END


def run_android(keys_root, key_dir, title, bundleID):
    out = os.path.join(key_dir, 'ios')
    mkdirs(out)

    def common(src, dst):
        cp(os.path.join(src, '*.props'), dst)
        cp(os.path.join(src, '*.keystore'), dst)

    # DEBUG START
    mkdirs(os.path.join(key_dir, 'android', 'debug'))
    debug_src = os.path.join(keys_root, 'android', 'debug')
    debug_dst = os.path.join(out, 'release')
    common(debug_src, debug_dst)
    # DEBUG END

    # RELEASE START
    mkdirs(os.path.join(key_dir, 'android', 'release'))
    release_src = os.path.join(keys_root, 'android', 'release')
    release_dst = os.path.join(out, 'release')
    common(release_src, release_dst)
    # RELEASE END


def run(keys_root, key_dir, title, bundleID):
    mkdirs(key_dir)
    run_ios(keys_root, key_dir, title, bundleID)
    run_android(keys_root, key_dir, title, bundleID)
    pass


def main():
    usage = """\
Usage:
    KeyHelper.py <keys.root> <key.dir> <title> [bundleID]
"""
    args = len(sys.argv)
    if args < 3:
        print(usage)
        exit(1)
    bundleID = sys.argv[4] if args > 4 else None
    run(sys.argv[1], sys.argv[2], sys.argv[3], bundleID)


if __name__ == '__main__':
    main()
